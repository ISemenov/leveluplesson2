package org.levelup.annotations;

import org.levelup.annotations.exceptions.InvalidPackageNameException;

import java.io.IOException;
import java.util.List;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Main {

    private static Logger log = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) throws IllegalAccessException, IOException, InstantiationException{

        LogManager.getLogManager().readConfiguration(RandomIntAnnotationProcessor.class.getResourceAsStream("/logging.properties"));

        RussianRoulette roulette = new RussianRoulette();
        RandomIntAnnotationProcessor.setField(roulette);
        System.out.println(roulette.getNumber());
        roulette.guess(4);

//      Invalid package name example
//      String packageName = "leveluplesson2.ru.level.up";
        String packageName = "ru.level.up";
        try {
            List<Object> objects = new PackageClassLoader().getObjects(packageName);
            for (Object object : objects) {
                System.out.println(object);
            }
        } catch (InvalidPackageNameException e) {
            log.throwing(Main.class.getName(), "getObjects", e);
        }
    }

}
