package org.levelup.annotations.exceptions;

public class PackageClassLoaderException extends Exception {

    public PackageClassLoaderException(String message) {
        super(message);
    }

    public PackageClassLoaderException(String message, Throwable cause) {
        super(message, cause);
    }

    public PackageClassLoaderException(Throwable cause) {
        super(cause);
    }
}
