package org.levelup.annotations.exceptions;

public class InvalidPackageNameException extends PackageClassLoaderException {

    public InvalidPackageNameException(String message) {
        super(message);
    }

    public InvalidPackageNameException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidPackageNameException(Throwable cause) {
        super(cause);
    }
}
